﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            int i = 0;
            //Random randomGen = new Random();

            for (i = 0; i < 20; i++)
            {
                //diceRoller.InsertDie(new Die(6,randomGen)); 
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();
            diceRoller.GetRollingResults();

            foreach (int result in diceRoller.ResultForEachRoll)
            {
                Console.WriteLine(result);
            }
        }
    }
}
